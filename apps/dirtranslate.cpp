#include "FTranslate.h"

using namespace ftranslate;

int main(int argc, char** argv) {
    if(argc < 3) {
        std::cerr << "Usage: " << argv[0] << " [src] [dest]\n";
        return 1;
    }
    
    const std::string src(argv[1]), dest(argv[2]);
    
    Spin::MemberInfo myInfo;
    myInfo.type = "fdtranslate";
    
    Spin::Initialize(Spinster::MemberID(5, 5041), myInfo);
    
    Seeker::Init();
    Chunk::Init();
    Request::Init();
    
    std::cout << "Translating the contents of " << src << " to " << dest << "...\n";
    
    //Cache dir: "tests/output/dbg/"
    Seeker *seeker = new Seeker(src, dest, dest + "/dbg/");
    
    seeker->search();
    
    seeker->printListing();
    //return 0;
    std::cout << "Beginning translation routine...\n";
    return !seeker->execute();
}
