#include "Seeker.h"
#include <ARKE/AppData.h>
#include "File.h"
#include "Intermediate.h"
#include "Request.h"

namespace ftranslate {
    Seeker::Seeker(const std::string& inPath, const std::string& outPath, const std::string& cPath) : wasAltered(false) {
        input.path = inPath;
        output.path = outPath;
        cache.path = cPath;
    }
    Seeker::~Seeker() {
        
    }
    
    
    bool Seeker::search() {
        bool *const altered = &(this->wasAltered);
        
        //Load cached intermediate results for sieve-based translation
        ::arke::Directory *inters = new ::arke::Directory(cache.path);
        
        inters->listCallback([&](std::string path, std::string fullPath)->bool {
            std::cout << "Found intermediate cache at " << path << " -> " << fullPath << "\n";
            
            Intermediate *inter = new Intermediate();
            bool good = inter->load(fullPath);
            intermediates.push_back(inter);
            
            if(!good) {
                std::stringstream cmdss;
                cmdss << "mv \"" << output.path << "/" << path << "\" \"" << output.path << "/" << path << ".old\" 2> /dev/null";
                system(cmdss.str().c_str());
                std::cout << "Invalidated cache for " << path << " (" << cmdss.str() << ")...\n";
                (*altered) = true;
                //sleep(1);
            }
            //usleep(200000);
            return true;
        }, true);
        
        ::arke::Directory *dir = new ::arke::Directory(input.path);
        dir->listCallback([&](std::string path, std::string fullPath)->bool {
            std::cout << "found file at " << path << " -> " << fullPath << "\n";
            std::string outPath = output.path + "/" + path;
            
            if(system(("stat '" + outPath + "' > /dev/null 2>&1").c_str()) == 0) {
                std::cout << "\toutput file exists, skipping it...\n";
                return true;
            }
            
            File *file = new File();
            file->path = fullPath;
            file->outputPath = outPath;
            file->debugPath = output.path + "/dbg/" + path;
            //Ensure the directory exists
            {
                std::stringstream cmd;
                cmd << "bash -c \"mkdir -p `dirname '" << file->outputPath << "'`\"";
                system(cmd.str().c_str());
            }
            {
                std::stringstream cmd;
                cmd << "bash -c \"mkdir -p `dirname '" << file->debugPath << "'`\"";
                system(cmd.str().c_str());
            }
            
            
            this->files.push_back(file);
            return true;
        }, true);
        return true;
    }
    
    void Seeker::printListing() {
        for(File *const& file : files) {
            std::wcout << file->path.c_str() << " -> " << file->outputPath.c_str() << " : " << file->debugPath.c_str() << "\n";
        }
    }
    
    bool Seeker::execute() {
        std::wcout << "Executing translation on " << files.size() << " files...\n";
        for(File *const& file : files) {
            std::wcout << "Translating " << file->path.c_str() <<  "...";
            std::wcout.flush();
            file->load();
            file->translate();
            file->debugSave();
            file->save();
            file->unload();
        }
        
        std::cout << "Retrieved a total of " << Request::SieveAdditions << " new translation records\n";
        
        std::cout << "Writing entire cache to disk, this could take a bit...\n";
        const std::string sievePath = cache.path + "cache.txt";
        bool cacheSaved = Request::SaveSieve(sievePath);
        
        if(cacheSaved) std::cout << "\tCache written to " << sievePath << "\n";
        else std::cerr << "\tFailed to write cache to " << sievePath << "\n";
        
        if(this->wasAltered) {
            std::cout << "WARNING: at least one file was invalidated during the last pass, recommend re-running to make sure all translation records were fixed.\n";
            for(auto it = Intermediate::InvalidatedFiles.begin(); it != Intermediate::InvalidatedFiles.end(); ++it) {
                std::cout << "\t" << (*it) << "\n";
            }
        }
        
        return true;
    }
    
    void Seeker::Init() {
        AddExt("txt");
    }
    void Seeker::AddExt(const std::string& ext) {
        exts.insert(ext);
    }
    
    std::set<std::string> Seeker::exts;
};
