#ifndef FTRANSLATE_FILE_H
#define FTRANSLATE_FILE_H
#include "iFTranslate.h"
#include <vector>
#include <list>
#include <set>
#include <fstream>
#include <mutex>

namespace ftranslate {
    class File {
    public:
        File(const std::string& targetPath = "");
        ~File();
        
        bool load(std::string targetPath = "");
        void unload();
        bool save(std::string outPath = "");
        
        bool debugSave(std::string dbgPath = "");
        
        bool complete();
        
        void translate();
        
        std::string path, outputPath, debugPath;
    protected:
        std::vector<Chunk*> chunks;
        std::set<Chunk*> incomplete;
        Chunk *edge;
        
        std::mutex statusMtx;
        
        std::wfstream fs;
    };
};

#endif

