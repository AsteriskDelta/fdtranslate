#ifndef FTRANSLATE_SEEKER_H
#define FTRANSLATE_SEEkER_H
#include "iFTranslate.h"
#include <vector>
#include <set>

namespace ftranslate {
    class Seeker {
    public:
        Seeker(const std::string& inPath, const std::string& outPath, const std::string& cPath);
        ~Seeker();
        
        bool search();
        
        void printListing();
        
        bool execute();
        
        static void Init();
        static void AddExt(const std::string& ext);
    protected:
        struct {
            std::string path;
        } input, output, cache;
        
        bool wasAltered;
        
        std::vector<File*> files;
        std::vector<Intermediate*> intermediates;
        
        static std::set<std::string> exts;
    };
};

#endif

