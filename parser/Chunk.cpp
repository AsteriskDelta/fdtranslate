#include "Chunk.h"
#include "Request.h"
#include <sstream>
#include <locale>
#include <codecvt>

namespace ftranslate {
    Chunk::Chunk() : needsTranslation(false), complete(false), translated(false), tagged(false) {
        
    }
    Chunk::~Chunk() {
        if(request != nullptr) delete request;
    }
    
    bool Chunk::empty() const {
        return from.str.empty();
    }
    
    //Returns true as long as we can take in the string
    bool Chunk::add(std::wstring dat) {
        if(this->empty()) {
            from.str += dat;
            
            if(Delimiters.find(dat) != Delimiters.end()) {
                this->tagged = true;
            } else if(Inconsequent.find(dat) == Inconsequent.end()) this->needsTranslation = true;
            
            return true;
        } 
        
        if(Delimiters.find(dat) != Delimiters.end() && !this->tagged) return false;//Break on delimeter
        else if(this->tagged &&Delimiters.find(dat) == Delimiters.end() && Inconsequent.find(dat) == Inconsequent.end()) return false;
        else if(!this->needsTranslation && !this->tagged && Inconsequent.find(dat) == Inconsequent.end()) this->needsTranslation = true;
        
        //std::wcout << "adding " << dat << "\n";
        from.str += dat;
        return true;
    }
    
    void Chunk::process() {
        request = new Request();
        
        request->from = from.str;
        request->execute();
        
        if(!request->complete()) to.str = from.str;//Failed, bail out safely
        else to.str = request->to;
        
        this->complete = true;
    }
    
    std::wstring Chunk::str() const {
       if(this->needsTranslation) return to.str;
       else return from.str;
    }
    std::wstring Chunk::debug() const {
        std::wstringstream ss;
        ss << "\"" << from.str << "\" -> \"" << to.str << "\"";
        return ss.str();
    }
    
    void Chunk::AddDelimiter(const std::wstring del) {
        Delimiters.insert(del);
    }
    void Chunk::AddInconsequent(const std::wstring ignore) {
        Inconsequent.insert(ignore);
    }
    
    std::set<std::wstring> Chunk::Delimiters;
    std::set<std::wstring> Chunk::Inconsequent;
    
    void Chunk::Init() {
        
        for(int i = 0; i < 128; i++) {
            bool delim = false, ignore = false;
            std::wstring patt;
            patt += static_cast<wchar_t>(i);
            
            if(i < 32) delim = true;
            
            //Ignore standard english characters and spacing, but not symbols/punctuation
            if(i == 32 || i == 39 ||(48 <= i && i <= 57) || (65 <= i && i <= 90) || (97 <= i && i <= 122)) ignore = true;
            else if(i == 39/* || i == 44*/) ignore = true;//Single quotes and commas
            else {
                delim = true;
            }
            
            if(delim) AddDelimiter(patt);
            if(ignore) AddInconsequent(patt);
        }
    }
};
