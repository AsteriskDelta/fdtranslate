#ifndef FTRANSLATE_CHUNK_H
#define FTRANSLATE_CHUNK_H
#include "iFTranslate.h"
#include <set>

namespace ftranslate {
    class Chunk {
    public:
        Chunk();
        ~Chunk();
        
        struct {
            size_t start, end;
            inline size_t length() const {
                return end - start;
            }
            std::wstring str;
        } from, to;
        
        bool empty() const;
        
        //Returns true as long as we can take in the string
        bool add(std::wstring dat);
        
        void process();
        
        std::wstring str() const;
        std::wstring debug() const;
        bool needsTranslation, complete, translated, tagged;
        
        static void AddDelimiter(const std::wstring del);
        static void AddInconsequent(const std::wstring ignore);
        static std::set<std::wstring> Delimiters;
        static std::set<std::wstring> Inconsequent;
        
        static void Init();
    protected:
        Request *request;
    };
};

#endif
