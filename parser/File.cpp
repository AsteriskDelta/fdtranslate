#include "File.h"
#include "Chunk.h"
#include <unistd.h>
#include <locale>
#include <codecvt>

namespace ftranslate {
    File::File(const std::string& targetPath) {
        if(!targetPath.empty()) this->load(targetPath);
    }
    File::~File() {
        for(Chunk *const& chunk : chunks) delete chunk;
    }
    
    bool File::load(std::string targetPath) {
        if(targetPath.empty()) targetPath = path;
        else path = targetPath;
        fs.open(targetPath, std::fstream::in | std::fstream::binary);
        fs.imbue(std::locale(fs.getloc(),
                             new std::codecvt_utf8<wchar_t, 0x10ffff, std::consume_header>));
        if(!fs) return false;
        
        while(!fs.eof()) {
            Chunk *newChunk = new Chunk();
            chunks.push_back(newChunk);
            this->edge = newChunk;
            
            while(!fs.eof()) {
                std::wstring word;
                wchar_t chr;
                fs.get(chr);
                if(fs.eof()) break;//Don't add a garbage value if we hit EOF on the current wchar
                
                word += chr;
                
                if(!newChunk->add(word)) {
                    //Skip to the next chunk
                    fs.putback(chr);
                    //std::wcout << "Begin new chunk at " << chr << "\n";
                    break;
                }
                
                //usleep(1000);
            }
            //std::wcout << "FS end\n";
            //usleep(1000*1000);
            
            //Chunk completed, tag if needed
            if(newChunk->needsTranslation) incomplete.insert(newChunk);
        }
        
        return true;
    }
    
    void File::unload() {
        try {
            fs.close();
        } catch(...) {};
    }
    
    bool File::save(std::string outPath) {
        if(outPath.empty()) outPath = outputPath;
        else outputPath = outPath;
        std::wfstream out(outPath, std::fstream::out | std::fstream::trunc | std::fstream::binary);
        out.imbue(std::locale(out.getloc(),
                             new std::codecvt_utf8<wchar_t, 0x10ffff, std::consume_header>));
        if(!out) return false;
        
        for(Chunk *const& chunk : chunks) {
            out << chunk->str();
        }
        out.close();
        return true;
    }
    
    bool File::debugSave(std::string dbgPath) {
        if(dbgPath.empty()) dbgPath = debugPath;
        std::wfstream out(dbgPath, std::fstream::out | std::fstream::trunc | std::fstream::binary);
        out.imbue(std::locale(out.getloc(),
                              new std::codecvt_utf8<wchar_t, 0x10ffff, std::consume_header>));
        if(!out) return false;
        
        for(Chunk *const& chunk : chunks) {
            if(!chunk->needsTranslation) continue;
            out << chunk->debug() << "\n";
        }
        out.close();
        return true;
    }
    
    bool File::complete() {
        statusMtx.lock();
        bool ret = incomplete.empty();
        statusMtx.unlock();
        return ret;
    }
    
    void File::translate() {
        //File *const f = this;
        for(Chunk *const& chunk : chunks) {
            if(chunk->complete || !chunk->needsTranslation) continue;
            
            std::mutex *mtx = &statusMtx;
            auto *set = &incomplete;
            
            Spin::Local->invoke([chunk]()->int {
                chunk->process();
                return 1;
            })->callback([mtx, set, chunk](int rtVal) {
                _unused(rtVal);
                mtx->lock();
                set->erase(chunk);
                mtx->unlock();
            });
        }
        
        std::cout << "\n";
        //Wait for everything to complete
        while(!incomplete.empty()) {
            std::wcout << "\r\t" << path.c_str() << " : " << (chunks.size() - incomplete.size()) << " / " << chunks.size() << "                      ";
            std::wcout.flush();
            usleep(3000);
        }
        std::cout << "\n";
    }
};
