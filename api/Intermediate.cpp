#include "Intermediate.h"
#include <locale>
#include <fstream>
#include <sstream>
#include <codecvt>
#include "Request.h"

namespace ftranslate {
    std::list<std::string> Intermediate::InvalidatedFiles;
    
    Intermediate::Intermediate(const std::string& fPath) {
        if(!fPath.empty()) this->load(fPath);
    }
    Intermediate::~Intermediate() {
        
    }
    
    //Loads and registers contents
    bool Intermediate::load(std::string filePath) {
        if(!filePath.empty()) path = filePath;
        
        std::wfstream fs;
        fs.open(path, std::fstream::in | std::fstream::out | std::fstream::binary);
        fs.imbue(std::locale(fs.getloc(),
                             new std::codecvt_utf8<wchar_t, 0x10ffff, std::consume_header>));
        if(!fs) return false;
        
        std::wstringstream ss; ss << "\" -> \"";
        const std::wstring divider(ss.str());
        
        ss.str(std::wstring());
        ss << "null";
        const std::wstring nullStr = ss.str();
        
        unsigned int cacheLines = 0, readLines = 0;
        bool invalidated = false;//If true, move the output file and rewrite it, since we have at least one invalid cache entry
        
        std::wstring line;
        while(std::getline<wchar_t>(fs, line)) {
            readLines++;
            //std::wcout << "#" << readLines << ", s" << line.size() << "\t\"" << line << "\"\n";
            if(line[0] != '"') {
                std::wcerr << "line #"<<readLines<< " in " << filePath.c_str() << " doesn't start with a quote (starts with \"" << line[0] << "\" in " << line << "), fix it yourself!\n"; 
                exit(1);
            }/* else if(line.find(nullStr) != std::string::npos) {
                std::cerr << "line #"<<readLines<< " in " << filePath << " is null or empty, wtf?!?!!\n"; 
                exit(1);
            }*/
            
            std::wstring from, to;
            size_t midPos = line.find(divider);
            //std::wcout << line << "\n";
            
            from = line.substr(1, midPos - 1);
            size_t toPos = midPos + divider.size();
            to = line.substr(toPos, line.size() - toPos - 1);
            
            if((to.empty() && !from.empty()) || 
                (to.find_first_not_of(' ') == std::string::npos && from.find_first_not_of(' ') != std::string::npos) ||
                (to.find(nullStr) == 0 && from.find(nullStr) != from.find_first_not_of(' ')) ||
                /*(to == from && to[0] > 127 && to.size() > 4)*/false//Nothing was actually translated, so redo it
            ) {
                invalidated = true;
                std::cerr << "line #"<<readLines<< " in " << filePath << " has an empty translation record:\n";
                std::wcerr << "\t" << line << "\n";
                continue;
            }
            
            //std::wcout << "\tcached: \"" << from << "\" -> \"" << Request::Escape(to) << "\"\n";
            //usleep(200000);
            if(Request::Sieve.find(from) == Request::Sieve.end()) {
                Request::Sieve[from] = to;
                cacheLines++;
            }
        }
        
        std::cout << "\tLoaded " << cacheLines << " cache entries\n";
        
        fs.close();
        if(invalidated) {
            InvalidatedFiles.push_back(filePath);
            std::cout << "\t\tWARNING: empty cache records, invalidating old output file to repair...\n";
            return false;
        } else return true;
    }
};
