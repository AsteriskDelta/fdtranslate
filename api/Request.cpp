#include "Request.h"
#include <locale>
#include <codecvt>
#include <sstream>
#include <algorithm>
#include <fstream>

namespace ftranslate {
    Request::Request() : id(ReqID++), completed(false) {
        
    }
    Request::~Request() {
        
    }
    
    bool Request::execute() {
        if(this->from.empty()) {//Skip empty requests
            this->to.clear();
            return true;
        }
        
        {
            SieveMtx.lock();//make sure we don't accidentally read an invalid, in-progress entry
            auto it = Sieve.find(this->from);
            SieveMtx.unlock();
            if(it != Sieve.end()) {
                this->to = it->second;
                this->completed = true;
                //std::wcout << "\tsieve hit!\n";
                return true;
            }
        }
        
        this->writeTmp();
        
        bool status = true;
        
        //Launch curl and wait for reply to load into 'to'
        std::stringstream cmdss;
        cmdss << "curl -s -X POST -H 'Content-Type: application/json' -H 'Authorization: Bearer " << ApiKey << "' 'https://translation.googleapis.com/language/translate/v2' -d @tmp/req" << this->id << ".json";
        
        cmdss << " | jq -r '.data.translations[0].translatedText' > tmp/resp" << this->id << ".txt";
        
        int reCode = system(cmdss.str().c_str());
        if(reCode != 0) {
            std::cerr << "Invalid authentication token, you need to renew it ;-;\n";
        }
        
        {
            std::wfstream fs("tmp/resp" + std::to_string(this->id) + ".txt", std::fstream::in | std::fstream::binary);
            fs.imbue(std::locale(fs.getloc(), new std::codecvt_utf8<wchar_t, 0x10ffff, std::consume_header>));
            std::wstringstream wss;
            wss << fs.rdbuf();
            this->to = wss.str();
            if(to.size() >= 4 && from.size() >= 4 && to[0] == 'n' && to[1] == 'u' && to[2] == 'l' && to[3] == 'l' && to.size() <= 5 && 
                !(from[0] == 'n' && from[1] == 'u' && from[2] == 'l' && from[3] == 'l' && from.size() <= 5)
            ) {
                //std::cerr << "Invalid authentication token, you need to renew it ;-;\n";
                this->to.clear();
            }
            
            if(*(this->to.rbegin()) == '\n') this->to = this->to.substr(0, to.size()-1);//Remove extra newline from shell echo
            fs.close();
            
            this->to = Escape(this->to);
            //Already done in Escape()
            //str.erase(std::remove(this->to.begin(), this->to.end(), '\n'), this->to.end());
            if(!this->to.empty()) {
                SieveMtx.lock();
                Sieve[this->from] = this->to;
                SieveAdditions++;
                SieveMtx.unlock();
            }
        }
        
        std::wcout << this->to << "\n";
        
        this->removeTmp();
        
        this->completed = true;
        
        return status;
    }
    
    bool Request::complete() const {
        return from.empty() || !to.empty();
    }
    
    bool Request::writeTmp() {
        std::wfstream fs("tmp/req"+std::to_string(this->id)+".json", std::fstream::out | std::fstream::trunc | std::fstream::binary);
        fs.imbue(std::locale(fs.getloc(),
                             new std::codecvt_utf8<wchar_t, 0x10ffff, std::consume_header>));
        
        fs << "{\n\t'q': \"" << Escape(this->from)  << "\",\n\t'source': 'ja',\n\t'target': 'en',\n\t'format': 'text'\n}";
        
        fs.close();
        return true;
    }
    void Request::removeTmp() {
        std::string cmds[2] = {"rm tmp/req" + std::to_string(this->id) + ".json 2>/dev/null", "rm tmp/resp" + std::to_string(this->id) + ".txt 2>/dev/null"};
        for(int i = 0; i < 2; i++) system(cmds[i].c_str());
    }

    void Request::Init() {
        
    }
    
    std::wstring Request::Escape(std::wstring in) {
        std::wstringstream ret;
        for(auto c : in) {
            if(c == '\"') ret << wchar_t('\\');
            else if(c == '\n') continue;
            ret << c;
        }
        return ret.str();
    }
    
    std::string Request::ApiKey = "";
    Uint64 Request::ReqID = 0;
    std::unordered_map<std::wstring, std::wstring> Request::Sieve;
    std::mutex Request::SieveMtx;
    unsigned long long Request::SieveAdditions = 0;
    
    bool Request::SaveSieve(const std::string& filePath) {
        std::lock_guard<std::mutex> lck(SieveMtx);
        
        std::wfstream out(filePath, std::fstream::out | std::fstream::trunc | std::fstream::binary);
        out.imbue(std::locale(out.getloc(), new std::codecvt_utf8<wchar_t, 0x10ffff, std::consume_header>));
        if(!out) return false;
        
        unsigned int writeCnt = 0, totalCnt = Sieve.size();
        
        for(auto it = Sieve.begin(); it != Sieve.end(); ++it) {
            out << "\"" << it->first << "\" -> \"" << it->second << "\"\n";
            writeCnt++;
            
            if(writeCnt % 100 == 0) {
                std::cout << " [" << (round(double(writeCnt)/double(totalCnt)*10000.0) / 100.0) << "%] " << writeCnt << " / " << totalCnt << "             \r";
                std::cout.flush();
            }
        }
        std::cout << "\n";
        
        out.close();
        
        return true;
    }
};
