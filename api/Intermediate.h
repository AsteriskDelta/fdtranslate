#ifndef FTRANSLATE_INTERMEDIATE_H
#define FTRANSLATE_INTERMEDIATE_H
#include "iFTranslate.h"
#include <list>

namespace ftranslate {
    class Intermediate {
    public:
        friend class Request;
        
        Intermediate(const std::string& fPath = "");
        ~Intermediate();
        
        //Loads and registers contents
        bool load(std::string filePath);
        
        std::string path;
        static std::list<std::string> InvalidatedFiles;
    protected:
    };
};

#endif
