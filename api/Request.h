#ifndef FTRANSLATE_REQUEST_H
#define FTRANSLATE_REQUEST_H
#include "iFTranslate.h"
#include <unordered_map>
#include <mutex>

namespace ftranslate {
    class Request {
    public:
        friend class Intermediate;
        
        Request();
        ~Request();
        
        Uint64 id;
        std::wstring from, to;
        
        bool execute();
        
        bool complete() const;
        
        static void Init();
        
        static bool SaveSieve(const std::string& filePath);
        static unsigned long long SieveAdditions;
    protected:
        bool writeTmp();
        void removeTmp();
        
        bool completed;
        
        static std::string ApiKey;
        static Uint64 ReqID;
        
        static std::wstring Escape(std::wstring in);
        
        static std::mutex SieveMtx;
        static std::unordered_map<std::wstring, std::wstring> Sieve;
    };
};

#endif
